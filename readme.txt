#########################
#  author: T.Tscherpel  #
#  created 04/07/2023   #
#########################

This file contains supplementary material for the article 
	DIMENSIONS OF EXACTLY DIVERGENCE-FREE FINITE ELEMENT SPACES IN 3D
	by L. R. SCOTT AND T. TSCHERPEL
	published in [...]

#########################

main file: meshquantities.py 
	to be run with FEniCS (dolfin)
	
	generation of 2 types of meshes:
            mesh1 (regular mesh of unit cube) and
            mesh2 (diamond mesh), with scaling factor s = 20
		figure(1): 	plot of mesh1
		saved files: 	"cube_mesh.png", "cube_mesh.tex" (tikz)
		figure(2): 	plot of mesh2 
		saved files: 	"diamond_mesh.png", "diamond_mesh.tex" (tikz)
	
	uniform refinement 
		figure(3): 	plot of ebar over v for uniformly refined meshes
		saved files: 	"ebar_unif.tex" (tikz), "ebar_unif.png"
	
	adaptive refinement (58 times)
		figure(4): 	plot of ebar over v for adaptively refined meshes with
                                refinement point refpt = [.1111111,.33333333,0.33333333]
		saved files: 	"ebar_adapt.tex" (tikz), "ebar_adapt.png"

#########################	
	
list of subroutines

	meshcube(iR,s)	
    		# input: iR initial refinement, s scaling factor leading to cube [0,s]^3
    		# output: regular mesh of unit cube 
    
	meshdiamond(m,s)
    		# input: m number of tetrahedra on the spine, s scaling factor for the whole mesh
    		# output: mesh of a diamond domain with spine vertices (0,0,s) and (0,0,-s) and
                          equator vertices on the circle with radius s, with 2m tetrahedra
    		
    	meshquant(mesh):
    		# input: mesh
    		# output: v number of vertices, e number of edges 
    		
	ebar(mesh,ind,maxref,vmax,refpt):
    		# input: mesh, ind indicator for uniform (0) or adaptive (1) refinement,
                         refmax maximal number of refinements, 
		  	 vmax maximal number of vertices, refpt point for adaptive refinement 
    		# output: vlist list of vertices, ebarlist list of ebar values of the mesh thats
                          arises by uniform or adaptive refinements from the inout mesh 

	adaptiveref(mesh,refpt):
		# soubtourine of ebar
    		# input:     mesh, refpt point towards which adaptive refinement is performed
    		# output:    refined mesh (using Carey-Plaza refinement)
    
